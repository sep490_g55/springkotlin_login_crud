package com.example.demo.service
import com.example.demo.model.UserRegistrationDto
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetailsService
interface UserService : UserDetailsService {
    fun save(registrationDto: UserRegistrationDto?): com.example.demo.entity.User?
    val all: List<com.example.demo.entity.User?>?
}